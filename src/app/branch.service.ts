import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import BranchesJson from '../assets/branches/branches.json';
import { IBranch, IBranchRoot } from './branches/branch';


@Injectable({
  providedIn: 'root'
})
export class BranchService {
  public branches: IBranch[];
  public branches2: IBranchRoot[];
  public branches3: IBranch[];

  constructor(private http: HttpClient) {
  }

  // Get from file directy
  getBranches(): IBranch[] {
    this.branches = BranchesJson;
    return this.branches;
  }

  // Get from api-service CORS by Herokuapp.com
  getOPBranches(): Observable<IBranchRoot> {
    const opHeaders = new HttpHeaders({'Content-Type' : 'application/json',
    'x-api-key': 'RpwQ17lhVrDsUvubS5bZksGVvSz76AqO' , 'x-authorization': 'w7pi74iT97CjpwTV'});

    return this.http.get<IBranchRoot>('https://cors-anywhere.herokuapp.com/https://sandbox.apis.op-palvelut.fi/branches/v1/branches',
     {headers: opHeaders})
     .pipe(
       retry(1), catchError(this.handleError)
     );
  }

  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}
