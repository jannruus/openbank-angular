import { Component, OnInit } from '@angular/core';
import { IBranch } from './branch';
import { BranchService } from '../branch.service';

@Component({
  templateUrl: './branch-list.component.html',
  styleUrls: ['./branch-list.component.css']
})
export class BranchListComponent implements OnInit {
  public pageTitle = 'Branch List';
  errorMessage = '';
  public branches: IBranch[];
  public filteredBranches: IBranch[];
  filter: string;
  page = 1;


  constructor(private branchService: BranchService) {
  }

  ngOnInit(): void {
    this.branchService.getOPBranches().subscribe(
      res => {this.filteredBranches = res.payload;
              this.branches = res.payload;
      },
      error => {
        this.errorMessage = error;
      });
  }

  onKey(filter: string) {
    console.log('haku kentän:' + filter);
    this.filteredBranches = filter ? this.performFilter(filter) : this.branches;
  }

  performFilter(filterBy: string): IBranch[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.branches.filter((branch: IBranch) =>
      branch.name.fi.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }

}
