/* OP OpenBank Mobilility -api */
export interface IBranch {
  info: ILang;
  name: ILang;
  town: ILang;
  street: ILang;
  location: ILocation;
  postcode: string;
  branchType: ILang;
  phoneNumber: string;
  openingHours: ILang;
}

export interface ILang {
  fi: string;
  sv: string;
}

export interface ILocation {
  type: string;
  coordinates?: (number) [] | null;
}

export interface IBranchRoot {
  payload: IBranch[];
}
